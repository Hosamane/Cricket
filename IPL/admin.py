from django.contrib import admin
from models import *

admin.site.register(Owner)
admin.site.register(Register)
admin.site.register(Like)
admin.site.register(Comment)
admin.site.register(Share)

